import csv
import json
import os
import ssl
import uuid
import asyncio
from io import StringIO, BytesIO
from collections import defaultdict

from aiohttp.client import ClientSession
from aiohttp.client_exceptions import ClientError
from fastapi import FastAPI, Request, Response

CH_HOST = os.getenv('LOGBROKER_CH_HOST', 'localhost')
CH_USER = os.getenv('LOGBROKER_CH_USER')
CH_PASSWORD = os.getenv('LOGBROKER_CH_PASSWORD')
CH_PORT = int(os.getenv('LOGBROKER_CH_PORT', 8123))
CH_CERT_PATH = os.getenv('LOGBROKER_CH_CERT_PATH')
TMP_LOG_DIR = "tmp_logs/"
BIN_LOG_DIR = "tmp_logs_bin/"

delay = 30

canceled = []

async def execute_query(query, data=None):
    url = f'http://{CH_HOST}:{CH_PORT}/'
    params = {
        'query': query.strip()
    }
    headers = {}
    if CH_USER is not None:
        headers['X-ClickHouse-User'] = CH_USER
        if CH_PASSWORD is not None:
            headers['X-ClickHouse-Key'] = CH_PASSWORD
    ssl_context = ssl.create_default_context(cafile=CH_CERT_PATH) if CH_CERT_PATH is not None else None

    async with ClientSession() as session:
        async with session.post(url, params=params, data=data, headers=headers, ssl=ssl_context) as resp:
            await resp.read()
            try:
                resp.raise_for_status()
                return resp, None
            except ClientError as e:
                return resp, {'error': str(e)}


app = FastAPI()

#  -> list_of_requsts, list_of_filenames 
async def read_all_from_disk():
    files = os.listdir(TMP_LOG_DIR)
    logs = []
    for file_name in files:
        with open(TMP_LOG_DIR + file_name) as f:
            logs.append(json.loads(f.read()))    
    return logs, files

async def read_all_from_disk_bin():
    files = os.listdir(BIN_LOG_DIR)
    logs = []
    for file_name in files:
        with open(BIN_LOG_DIR + file_name) as f:
            logs.append(f.read())
    return logs, files

#list_of_requsts -> list_of_jsons, list_of_lists merged by tablename
async def merge_csv_and_list(requests, filenames):
    global canceled
    answer_json = defaultdict(list)
    answer_list = defaultdict(list)
    bad_schema = []
    for r, f in zip(requests, filenames):
        for log_entry in r:
            table_name = log_entry.get('table_name')
            rows = log_entry.get('rows')
            if (not table_name or not rows):
                bad_schema.append(f)
                break

            if log_entry.get('format') == 'list':
                answer_list[table_name] += rows
            elif log_entry.get('format') == 'json':
                answer_json[table_name] += rows
            else:
                bad_schema.append(f)
                break
    
    canceled += bad_schema
    print("canceled: " + str(bad_schema), flush=True)
    return answer_list, answer_json
    
async def delete_from_disk(filenames, directory=TMP_LOG_DIR):
    for f in filenames:
        os.remove(directory + f)

async def read_and_send():
    while True:
        await asyncio.sleep(delay)
        requests, list_of_filenames = await read_all_from_disk()
        request_lists, request_jsons = await merge_csv_and_list(requests, list_of_filenames)
        print(f"Going to push list : {request_lists} \n json : {request_jsons}", flush=True)
        tasks = []
        for table_name, rows in request_lists.items():
            tasks.append(asyncio.create_task(send_csv(table_name, rows)))
        for table_name, rows in request_jsons.items():
            tasks.append(asyncio.create_task(send_json_each_row(table_name, rows)))
        all_submitted = True
        for task in tasks:
            if (await task != ""):
                all_submitted = False
        if all_submitted:
            await delete_from_disk(list_of_filenames)


        requests, list_of_filenames = await read_all_from_disk_bin()
        
        if requests:
            bin_requests = requests[0]
            for request in requests[1:]:
                bin_requests += request

        await delete_from_disk(list_of_filenames, directory=BIN_LOG_DIR)


            

asyncio.get_running_loop().create_task(read_and_send())

async def query_wrapper(query, data=None):
    res, err = await execute_query(query, data)
    if err is not None:
        return err
    return await res.text()


@app.get('/show_create_table')
async def show_create_table(table_name: str):
    resp = await query_wrapper(f'SHOW CREATE TABLE "{table_name}";')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp


async def send_csv(table_name, rows):
    data = StringIO()
    cwr = csv.writer(data, quoting=csv.QUOTE_ALL)
    cwr.writerows(rows)
    data.seek(0)
    return await query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT CSV', data)


async def send_json_each_row(table_name, rows):
    data = StringIO()
    for row in rows:
        assert isinstance(row, dict)
        data.write(json.dumps(row))
        data.write('\n')
    data.seek(0)
    return await query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT JSONEachRow', data)

def flush_on_disk(log_to_write, binary=False):
    directory = TMP_LOG_DIR
    if binary:
        directory = BIN_LOG_DIR
    unique_name = str(uuid.uuid4())
    with open(directory + unique_name, 'a') as f:
        f.write(log_to_write)
    print("flusshed " + log_to_write + " and uuid = " + unique_name, flush=True)
    return unique_name

@app.post('/write_log')
async def write_log(request: Request):
    body = await request.json()
    uuid = flush_on_disk(json.dumps(body))
    return uuid


@app.get('/healthcheck')
async def healthcheck():
    return Response(content='Ok', media_type='text/plain')

@app.get('/show_table_content')
async def show_table_content(table_name: str):
    resp = await query_wrapper(f'SELECT (*) FROM {table_name};')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp

@app.get('/drop_table')
async def drop_table(table_name: str):
    resp = await query_wrapper(f'DROP TABLE {table_name};')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp

@app.get('/get_status')
async def drop_table(request_id: str):
    global canceled

    if request_id in canceled:
        return json.dumps({'status': 'Canceled'})
    elif os.path.exists(TMP_LOG_DIR + request_id):
        return json.dumps({'status': 'Waiting'})
    else:
        return json.dumps({'status': 'Submitted'}) 



@app.post('/write_log')
async def write_log(request: Request):
    body = await request.json()
    uuid = flush_on_disk(json.dumps(body))
    return uuid


@app.post('/write_rawbinary_log')
async def test(request: Request):
    body = str(await request.json())
    uuid = flush_on_disk(body, binary=True)
    return uuid
# sudo docker-compose run curl logbroker:8000/drop_table?table_name=kek -v
# sudo docker-compose run clickhouse-client -q 'create table kek (a Int32, b String) ENGINE = MergeTree() primary key a;'
# sudo docker-compose run curl logbroker:8000/show_table_content?table_name=kek -v

# sudo docker-compose run curl logbroker:8000/write_log -d '[{"table_name": "kek", "rows": [{"a":1, "b": "some new row"}, {"a": 2}], "format": "json"}, {"table_name": "kek", "rows": [[1, "row from list"]], "format": "list"}, {"table_name": "mek", "rows": [[1, "row from list"]], "format": "list"}]' -v

# sudo docker-compose run curl logbroker:8000/get_status?request_id=5f73fdbe-c384-4955-9b1f-efdda4b5037c -v


# sudo docker-compose run curl logbroker:8000/write_log -d '[{"": ""}]' -v




